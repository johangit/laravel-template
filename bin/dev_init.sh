#!/bin/bash

echo "WEBSITE INITIALIZATION"

ROOTDIR=$(pwd)


docker run --rm -v $(pwd)/backend:/app composer install --ignore-platform-reqs


docker run --rm -v $(pwd)/backend:/app -w /app node:alpine yarn install

docker run --rm -v $(pwd)/frontend:/app -w /app node:alpine yarn install
docker run --rm -v $(pwd)/frontend:/app -w /app node:alpine yarn build


docker-compose up -d --build

docker-compose exec lt_backend php artisan key:generate
docker-compose exec lt_backend php artisan migrate --force

docker-compose stop