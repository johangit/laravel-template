@servers(['web' => 'laravel_test@89.223.95.225'])


@setup
    $repository = 'git@gitlab.com:johangit/laravel-template.git';
    $releases_dir = '/home/laravel_test/site/releases';
    $app_dir = '/home/laravel_test/site';
    $release = date('Y_m_d__H_i_s');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    run_composer
    frontend_build
    update_symlinks
    restart_react
@endstory


@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    docker run --rm -v $(pwd)/backend:/app composer install --ignore-platform-reqs -o --prefer-dist --no-scripts
@endtask

@task('frontend_build')
    echo "Building frontend"
    cd {{ $new_release_dir }}

    echo "Laravel fallback"
    docker run --rm -v $(pwd)/backend:/app -w=/app node:alpine yarn install

    echo "React app"
    docker run --rm -v $(pwd)/frontend:/app -w=/app node:alpine yarn install
    docker run --rm -v $(pwd)/frontend:/app -w=/app node:alpine yarn build
@endtask

@task('restart_react')
    echo "React restarting"
    cd {{ $new_release_dir }}

    docker-compose restart lt_frontend
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/backend/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/backend/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/backend/public/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/backend/.env

    echo 'Linking docker-compose.yml file'
    ln -nfs {{ $app_dir }}/docker-compose.yml {{ $new_release_dir }}/docker-compose.yml

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask